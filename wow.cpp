#include <iostream>
#include <vector>
#include <string>
using namespace std;

class CWarrior
{
  public:
    int index;
    int damage;
    virtual int GetStrength() = 0;
    virtual int Copy() = 0;
};

class CDragon : public CWarrior
{
  public:
    static int strength;
    virtual int GetStrength() { return strength; }
};
int CDragon::strength = 0;

class CNinja : public CWarrior
{
  public:
    static int strength;
    virtual int GetStrength() { return strength; }
};
int CNinja::strength = 0;

class CIceman : public CWarrior
{
  public:
    static int strength;
    virtual int GetStrength() { return strength; }
};
int CIceman::strength = 0;

class CLion : public CWarrior
{
  public:
    static int strength;
    virtual int GetStrength() { return strength; }
};
int CLion::strength = 0;

class CWolf : public CWarrior
{
  public:
    static int strength;
    virtual int GetStrength() { return strength; }
};
int CWolf::strength = 0;

class CHeadquarter
{
  private:
    vector<CWarrior> warriors;
    int strengthSrc;
    vector<CWarrior *> summonSq;
    vector<CWarrior *>::iterator newWarriorIndex;

  public:
    CHeadquarter(int s, const vector<CWarrior *> &l) : strengthSrc(s), summonSq(l)
    {
        newWarriorIndex = summonSq.begin();
    }
    CWarrior *MakeWarrior();
};

CWarrior *
CHeadquarter::MakeWarrior()
{
    return NULL;
}

int main(int argc, char *argv[])
{
    int nCase;
    cin >> nCase;
    for (; nCase > 0; --nCase)
    {
        int strengthSource;
        cin >> strengthSource;
        cin >> CDragon::strength >> CNinja::strength >> CIceman::strength;
        cin >> CLion::strength >> CWolf::strength;
        CDragon dragon;
        CNinja ninja;
        CIceman iceman;
        CLion lion;
        CWolf wolf;
        vector<Warrior *> redSq = {&iceman, &lion, &wolf, &ninja, &dragon};
        vector<Warrior *> blueSq = {&lion, &dragon, &ninja, &iceman, &wolf};
        Headquarter red(strengthSource, redSq);
        Headquarter blue(strengthSource, blueSq);
        CWarrior *w = red.MakeWarrior();
    }
    return 0;
}
